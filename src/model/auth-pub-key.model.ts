import { ObjectId } from 'mongodb';

interface IKey {
    n: string;
    e: string;
    kty: string;
    kid: string;
    alg: string;
    use: string;
}

export default class AuthPubKeyModel {
    constructor(
        public kid: string,
        public key: IKey,
        public iss: string,
        public ver: string = 'v1',
        public _id?: ObjectId,
    ) {}
}