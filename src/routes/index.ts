import express from "express";
import AuthCheckMiddleware from "../middleware/auth-check.middleware";
import userRouter from "./user-route";


export default function registerApiRoute(app: express.Application) {
    app.get("/", async (req: any, res: any) => {
        return res.send("OK");
    });

    app.get("/api/get-coins", AuthCheckMiddleware, (req, res) => {
        return res.send("all coins");
    });

    app.get("/api/get-coin-price", AuthCheckMiddleware, (req, res) => {
        return res.send("coin price for ");
    });

    app.use('/api/user', userRouter);
}