import { Router } from 'express';

import userController from '../controller/user-controller';

const userRouter = Router();

userRouter.route('/auth').post(userController.authAction);
// userRouter.route('/testendpoint').get(userController.testendpoint);

export default userRouter;