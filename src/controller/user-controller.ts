import { Request, Response } from 'express';
// import jwt from 'jsonwebtoken';

import * as jose from 'jose';

import UserService from '../service/user.service';
import authService from '../service/auth.service';

import AuthPubKeyModel from '../model/auth-pub-key.model';
// import UserModel from 'src/model/user.model';

class UserController {

    

    async loginAction(req: Request, res: Response) {
        
    }

    async authAction(req: Request, res: Response) {
        let keyModel:AuthPubKeyModel, payload: jose.JWTPayload;
        const token = req.body.credential;
        
        try {
            const protectedHeader = jose.decodeProtectedHeader(token);
            const kid = protectedHeader.kid;
            keyModel = await authService.getPublicKey(kid);
        } catch(err) {
            console.log(err);
            return res.status(400).send('invalid token');
        }

        const key = keyModel.key;
        try {
            const importedKey = await jose.importJWK({
                kty: key.kty,
                e: key.e,
                n: key.n,
                alg: key.alg,
            });

            const decoded = await jose.jwtVerify(token, importedKey);
            payload = decoded.payload;
        } catch(err) {
            console.log(err);
            return res.status(400).send('invalid signature');
        }

        let userModel = await UserService.getByRemoteId(payload.sub);
        let id;
        if(!userModel) {
            id = await UserService.create({
                sub: payload.sub,
                issuer: 'google',
                name: payload.name,
                pic: payload.picture
            });

            console.log(id);
        } else {
            id = userModel._id.toString();
        }
        // console.log(userModel);


        const privateSecret = new Uint8Array([0x62, 0x69, 0x74, 0x63, 0x6f, 0x69, 0x6e]);

        const jwt = await new jose.SignJWT({
            id: id,
            name: payload.name,
            pic: payload.picture
        })
        .setProtectedHeader({
            alg: 'HS256',
            kid: '999abc'
        })
        .setIssuedAt()
        .setIssuer('idp.cointracker.app')
        .sign(privateSecret);


        return res.status(200).send(jwt);
    }

    logoutAction(req: Request, res: Response) : Response {
        return res;
    }

    async testendpoint(req: Request, res: Response) {
        // const result = await axios.get('https://accounts.google.com/.well-known/openid-configuration');
        // const jwksUri = result.data.jwks_uri;

        // const pubkeyResults = await axios.get(jwksUri);


        // // console.log(pubkeyResults.data);
        // // console.log(result.data.jwks_uri);

        // const pubkeys: Array<any> = pubkeyResults.data.keys;
        // const key = pubkeys.find((k) => k.kid === 'caabf6908191616a908a1389220a975b3c0fbca1');
        // console.log(typeof(key));
        // if (!key) {
        //     throw new Error('public key not found from Google!');
        // }


        // let keyModel = new AuthPubKeyModel('caabf6908191616a908a1389220a975b3c0fbca1', key, 'google');






        // const keySaveResult = await savePublicKey(keyModel);

        // if(keySaveResult === true) {
        //     res.send(key);
        // } else {
        //     throw new Error(keySaveResult);
        // }

        
    }
}

const userController = new UserController();

export default userController;