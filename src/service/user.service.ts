import { Db } from "mongodb";
import { getDb } from './conn.service';
import UserModel from '../model/user.model';

function randomName(name: string) {
    return name + '-' + Math.floor(Math.random() * 100);
}

export default {
    // db: Db;
    // constructor() {
    //     console.log('user service created!');
    // }

    // async init() {
    //     try {
    //         this.db = await getDb();
    //         return this;
    //     } catch(err) {
    //         console.log(err);
    //         throw err;
    //     }
    // }

    async create(post: Object) {
        const db = await getDb();
        try {
            // let rn = randomName('test');
            let result = await db.collection('user').insertOne(post);
            return { success: true, result }
        } catch(err) {
            return { success: false, err }
        }
    },

    async getByRemoteId(remoteId: string) {
        const db = await getDb();
        try {
            const userDoc = await db.collection('user').findOne({sub: remoteId});
            return userDoc;
        } catch(err) {
            return null;
        }
    }
}

// export default UserService;