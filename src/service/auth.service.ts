import axios from 'axios';
import { Db, ObjectId } from "mongodb";

import { getDb } from './conn.service';

import AuthPubKeyModel from "../model/auth-pub-key.model";

// const PEM_URI = "https://www.googleapis.com/oauth2/v1/certs";

const GOOGLE_WELL_KNOWN_URI = "https://accounts.google.com/.well-known/openid-configuration";

export default {
    async savePublicKey(keyModel: AuthPubKeyModel, avoidDuplicate: true) {
        const db = await getDb();
        if(avoidDuplicate) {
            /**
             * @todo test for duplicate keyid
             */
        }

        try {
            const result = await db.collection('auth_pub_key').insertOne(keyModel);
            return true;
        } catch(err) {
            return err;
        }
    },

    async getPublicKey(kid: string): Promise<AuthPubKeyModel> {
        const db = await getDb();
        let keyModel:AuthPubKeyModel  = await db.collection<AuthPubKeyModel>('auth_pub_key').findOne(
            { kid },
            { projection: { _id: 0 } }
        );

        // key not found in db
        if(!keyModel) {
            // console.log('~~~~~~~~~~~~~~~~~~load from remote~~~~~~~~~~~~~~~~~~');
            const result = await axios.get(GOOGLE_WELL_KNOWN_URI);
            const jwksUri = result.data.jwks_uri;

            if(!jwksUri) {
                throw new Error('google well known endpoint err');
            }

            /**
             * @todo unable to use jwks for verify token, need more documents ....
             */

            const pubkeyResults = await axios.get(jwksUri);
            // const data = pubkeyResults.data;

            // if(typeof data !== 'object') {
            //     throw new Error("google pem end point error");
            // }
            
            // let key: string = pubkeyResults.data[kid];;
            
            const pubkeys: Array<any> = pubkeyResults.data.keys;


            console.log(pubkeys);


            const key = pubkeys.find((k) => k.kid === kid);

            if (!key) {
                throw new Error('public key not found from Google!');
            }


            keyModel = new AuthPubKeyModel(kid, key, 'google');


            console.log(keyModel);
            console.log('~~~~');

            const modelResult = await this.savePublicKey(keyModel, false);
            /**
             * @todo test the result !!! and handle err
             */
        }

        return keyModel;
    }
}

//  new AuthService();