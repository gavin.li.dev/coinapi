import { MongoClient, Db } from "mongodb";




let dbConnection: Db = null;

async function connect() {
  // console.log(process.env);

  const host = process.env.DB_HOST;
  const user = process.env.DB_USER;
  const pass = process.env.DB_PASS;
  const dbName = process.env.DB_NAME;

  const uri = `mongodb+srv://${user}:${pass}@${host}/?retryWrites=true&w=majority`;


  // console.log('~~');

  // console.log(host);
  // console.log('~~');
  // console.log(process.env.DB_HOST)
  // console.log('~~');
  // console.log(uri);
  const client = new MongoClient(uri);

  try {
    console.log('try to create new db connection!');

    await client.connect();

    dbConnection = client.db(dbName);

    // console.log('connected to db');

    return dbConnection;

  } catch(err) {
    console.log(`MongoDB connection failed with error: ${err}`);
  }

}

async function getDb() {
  if(dbConnection === null) {
    dbConnection = await connect();
  }
  return dbConnection;
}


export { connect, getDb }


// module.exports = {
//   connectToServer: function (callback) {
//     client.connect(function (err, db) {
//       if (err || !db) {
//         return callback(err);
//       }

//       dbConnection = db.db("sample_airbnb");
//       console.log("Successfully connected to MongoDB.");

//       return callback();
//     });
//   },

//   getDb: function () {
//     return dbConnection;
//   },
// };