import express from "express";
import * as jose from 'jose';
// import passport from "passport";

// passport.serializeUser((u, done) => {
//     console.log(u);
//     done(null, u);
// });


export default async function AuthCheckMiddleware(request: express.Request, response: express.Response, next: express.NextFunction) {
    const authHeader = request.headers['authorization'];
    console.log('~~~~~~~~~~~~');
    console.log(authHeader);
    console.log('~~~~~~~~~~~~');
    if(typeof authHeader != 'string') {
        response.sendStatus(403).send();
    } else {
    
        const bearers = authHeader.split(' ');
        if(bearers.length != 2 || bearers[0] != 'bearer') {
            response.sendStatus(403).send();
        } else {

            const secret = new Uint8Array([0x62, 0x69, 0x74, 0x63, 0x6f, 0x69, 0x6e, 1]);


            const token = bearers[1];



            try {
                const result = await jose.jwtVerify(token, secret);
            } catch(e) {
                console.log(e)
            }
            // console.log(result);

            response.locals.testu = token;
            response.setHeader('test-token', token);
            
            next();
        }
    }
}