import { Request, Response, NextFunction } from 'express';
// import { MongoClient, ServerApiVersion } from 'mongodb';
// import dotenv from "dotenv";

import { connect } from '../service/conn.service';

// const config = dotenv.config();

// const env = process.env.NODE_ENV;
// const port = process.env.SERVER_PORT;



// const uri = "mongodb+srv://nonstoprunner:<password>@cluster0.ngbb0p3.mongodb.net/?retryWrites=true&w=majority";
// const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true, serverApi: ServerApiVersion.v1 });
// client.connect(err => {
//   const collection = client.db("test").collection("devices");
//   // perform actions on the collection object
//   client.close();
// });

export default async function DbConnectMiddleware(request: Request, response: Response, next: NextFunction) {

    console.log('db middleware ran')
    await connect();

    // console.log('======');
    // console.log(config);
    // console.log('======');
    return next();
}