import bodyParser from "body-parser";
import express, { NextFunction } from "express";

import dotenv from "dotenv";
import helmet from 'helmet';
import cors from 'cors';
// import { connect } from './service/conn.service';

import DbConnectMiddleware from "./middleware/db-connect.middleware";
// import UserAuthMiddleware from "./middleware/auth-check.middleware";
import registerApiRoute from "./routes";

dotenv.config();

const env = process.env.NODE_ENV;
const port = process.env.SERVER_PORT;

// const express = require ("express");
// await connect(); 
const app = express();

const corsOptions = {
    origin: 'http://localhost:3000'
}

app.use(cors(corsOptions), helmet());
app.use(bodyParser.json());
// app.use((req, res, next) => {
//     res.header("Access-Control-Allow-Origin", "*");
//     // res.header("Allow", "GET,POST,PUT");
//     // res.content
//     next();
// });

app.use(DbConnectMiddleware);

registerApiRoute(app);

// app.use((error: Error, req: Request, res: Response, next: NextFunction) => {
//     console.log(error);
//     res.status(500).json(error);
// });
  

// app.get("/", (req, res) => {
//     res.send("OK 1");
// });

app.listen( port, () => {
    console.log(`${env} API server running on port ${port}`);
    console.log(`--------------------------------------------`);
});